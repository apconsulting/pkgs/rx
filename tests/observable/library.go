package main

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/apconsulting/pkgs/rx"
)

func TestStringObs() rx.IObservable[string] {
	return rx.NewObservable(func(sub *rx.Subscriber[string]) func() {
		sub.Next("Primo")
		sub.Next("Secondo")
		sub.Next("Terzo")

		go func() {
			select {
			case <-sub.Disposed:
				sub.Complete()
				return
			case <-time.After(3 * time.Second):
				sub.Next("Quarto ASYNC")
				sub.Complete()
			}
		}()

		return nil
	})
}

func TestIntObs() rx.IObservable[int] {
	return rx.NewObservable(func(sub *rx.Subscriber[int]) func() {
		sub.Next(1)
		sub.Next(2)
		sub.Next(3)

		go func() {
			select {
			case <-sub.Disposed:
				sub.Complete()
				return
			case <-time.After(3 * time.Second):
				sub.Next(4)
				sub.Complete()
			}
		}()

		return nil
	})
}

func TestObserver[T any](id string, wg *sync.WaitGroup) *rx.Observer[T] {
	return rx.NewObserver(
		func(val T) {
			fmt.Printf("%s) NEXT: %v\n", id, val)
		},
		rx.WithErrorFunc[T](
			func(err error) {
				fmt.Printf("%s) ERROR: %s\n", id, err)
				if wg != nil {
					wg.Done()
				}
			},
		),
		rx.WithCompleteFunc[T](
			func() {
				fmt.Printf("%s) COMPLETE\n", id)
				if wg != nil {
					wg.Done()
				}
			},
		),
	)
}

func TestTapping[T any](name string, tapNext bool) rx.PipeableOperatorFunc[T] {
	return rx.Tap(
		rx.TapOnSubscribe[T](func() {
			fmt.Printf("%s -- Tapped Subscribe\n", name)
		}),
		rx.TapOnComplete[T](func() {
			fmt.Printf("%s -- Tapped Completed\n", name)
		}),
		rx.TapOnUnsubscribe[T](func() {
			fmt.Printf("%s -- Tapped Unsubscribe\n", name)
		}),
		rx.TapOnNext(func(val T) {
			if tapNext {
				fmt.Printf("%s -- Tapped Next: %v\n", name, val)
			}
		}),
		rx.TapOnError[T](func(err error) {
			fmt.Printf("%s -- Tapped Error: %s\n", name, err)
		}),
	)
}

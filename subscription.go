package rx

import "sync/atomic"

type Subscription struct {
	// Indicates whether or not the Subscriber is still subscribed.
	isDisposed atomic.Bool

	// Disposed is a notification channel that closes when the Subscriber unsubscribes. It
	// may be used by async producers in a select{} statement to be notified.
	Disposed chan struct{}

	// Collection of callbacks to be executed when Unsubscribing.
	finalizers []func()
}

// Unsubscribes from this Subscription, closing Disposed channel and executing all
// finalizers
func (s *Subscription) Unsubscribe() {
	if !s.isDisposed.Swap(true) {
		close(s.Disposed)

		for _, teardown := range s.finalizers {
			teardown()
		}
	}
}

// Add a callback to the collection of finalizers. Execution order is respected when
// Unsubscribing
func (s *Subscription) AddTeardownLogic(f func()) {
	if !s.isDisposed.Load() {
		s.finalizers = append(s.finalizers, f)
	}
}

func NewSubscription() *Subscription {
	return &Subscription{
		Disposed:   make(chan struct{}),
		finalizers: make([]func(), 0),
	}
}

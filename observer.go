package rx

type IObserver[T any] interface {
	Next(T)
	Error(error)
	Complete()

	// setNextFunc(func(val T))
	// setErrorFunc(func(err error))
	// setCompleteFunc(func())
}

type Observer[T any] struct {
	nextFunc     func(val T)
	errorFunc    func(err error)
	completeFunc func()
}

func (o *Observer[T]) Next(val T) {
	o.nextFunc(val)
}
func (o *Observer[T]) Error(err error) {
	if o.errorFunc != nil {
		o.errorFunc(err)
	}
}
func (o *Observer[T]) Complete() {
	if o.completeFunc != nil {
		o.completeFunc()
	}
}
func (o *Observer[T]) setNextFunc(f func(val T)) {
	o.nextFunc = f
}
func (o *Observer[T]) setErrorFunc(f func(err error)) {
	o.errorFunc = f
}
func (o *Observer[T]) setCompleteFunc(f func()) {
	o.completeFunc = f
}

// func NewObserver[T any](nextFunc func(val T), errorFunc func(err error), completeFunc func()) *Observer[T] {
// 	if nextFunc == nil {
// 		panic("nextFunc can't be nil")
// 	}

// 	return &Observer[T]{
// 		nextFunc:     nextFunc,
// 		errorFunc:    errorFunc,
// 		completeFunc: completeFunc,
// 	}
// }

func WithNextFunc[T any](f func(val T)) ObserverFuncOption[T] {
	return func(o *Observer[T]) {
		o.setNextFunc(f)
	}
}
func WithErrorFunc[T any](f func(err error)) ObserverFuncOption[T] {
	return func(o *Observer[T]) {
		o.setErrorFunc(f)
	}
}
func WithCompleteFunc[T any](f func()) ObserverFuncOption[T] {
	return func(o *Observer[T]) {
		o.setCompleteFunc(f)
	}
}

type ObserverFuncOption[T any] func(*Observer[T])

func NewObserver[T any](nextFunc func(val T), opts ...ObserverFuncOption[T]) *Observer[T] {
	newObserver := &Observer[T]{
		nextFunc: nextFunc,
	}

	for _, opt := range opts {
		opt(newObserver)
	}

	return newObserver
}

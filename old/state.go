package rx

import "github.com/reactivex/rxgo/v2"

type State struct {
	obs     rxgo.Observable
	version int
}

package rx

import "sync/atomic"

func CatchError[T any](selector func(error, IObservable[T]) IObservable[T]) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription
			var switched atomic.Bool

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					if !switched.Load() {
						s.Next(val)
					}
				},
				errorFunc: func(err error) {
					var newSub *Subscription
					if !switched.Swap(true) {
						selector(err, CatchError(selector)(o)).operatorSubscribe(NewObserver(
							func(val T) {
								s.Next(val)
							},
							WithErrorFunc[T](func(err error) {
								s.Error(err)
							}),
							WithCompleteFunc[T](func() {
								s.Complete()
							}),
						), &newSub, s)
					}
				},
				completeFunc: func() {
					if !switched.Load() {
						s.Complete()
					}
				},
			}, &sub, s)

			return nil
		})
	})
}

package rx

type IObservable[T any] interface {
	Subscribe(IObserver[T]) *Subscription
	Pipe(...PipeableOperator[T]) IObservable[T]

	operatorSubscribe(IObserver[T], **Subscription, *Subscriber[T])
	onSubscribe(*Subscriber[T]) func()
}

// Concrete implementation of the IObservable interface
type Observable[T any] struct {
	onSubscribeFunc func(*Subscriber[T]) func()
}

func (o *Observable[T]) onSubscribe(sub *Subscriber[T]) func() {
	tdFunc := o.onSubscribeFunc(sub)
	return tdFunc
}

// Creates a cold observable from a producer function. The producer is run synchronously when
// an observer subscribes. If the producer goes async by starting goroutines, it can use the
// Subscriber.Unsubscribed channel to be notified when the subscriber unsubscribes.
func NewObservable[T any](producer func(*Subscriber[T]) func()) IObservable[T] {
	return &Observable[T]{
		onSubscribeFunc: producer,
	}
}

// subscribeWithTeardown is a function that subscribes to o using obs and adds a teardown
// logic to downstreamSubscriber to propagate Unsubscribe() events upstream. The newly
// created subscription is assigned to the in/out parameter sub, so it is available to the
// Observer asap
func subscribeWithTeardown[T, R any](o IObservable[T], obs IObserver[T], sub **Subscription, downstreamSubscriber *Subscriber[R]) {
	// TODO: Per via di come funzionano i generics, devo specificare sia il tipo del
	// downstreamSubscriber che sto trattando, sia quello dell'Observable al quale sto
	// sosttoscrivendo l'Observer, dato che potenzialmente possono essere diversi. Di
	// conseguenza non posso usare un metodo di Observable, dato che i metodi non permettono
	// di specificare ulteriori parametri di tipo, ma invece devo usare una funzione con
	// questa forma, dove il primo parametro fa in sostanza da "receiver" del metodo.

	if *sub == nil {
		newSub := NewSubscription()
		*sub = newSub
	}

	newSubscriber := Subscriber[T]{
		obs:          obs,
		Subscription: *sub,
	}

	downstreamSubscriber.AddTeardownLogic(newSubscriber.Unsubscribe)

	o.onSubscribe(&newSubscriber)
}

// Method that wraps subscribeWithTeardown when downstream and current Observable have the
// same type (mainly an helper method for PipeableOperators)
func (o *Observable[T]) operatorSubscribe(obs IObserver[T], sub **Subscription, downstreamSubscriber *Subscriber[T]) {
	subscribeWithTeardown[T](o, obs, sub, downstreamSubscriber)
}

// Connects an Observer's callbacks to an Observable by creating a Subscriber, then calls
// the Observable's onSubscribe logic with the new Subscriber. Returns the newly created
// Subscription.
func (o *Observable[T]) Subscribe(obs IObserver[T]) *Subscription {
	newSubscriber := Subscriber[T]{
		obs:          obs,
		Subscription: NewSubscription(),
	}

	// TODO: Vedere cosa fare con la funzione ritornata da Subscribe
	// if tdFunc := o.onSubscribe(&newSubscriber); tdFunc != nil {
	// 	newSubscriber.AddTeardownLogic(tdFunc)
	// }

	o.onSubscribe(&newSubscriber)

	return newSubscriber.Subscription
}

// Applies a list of PipeableOperators to the Observable, returning the resulting
// Observable. All operators must satisfy the PipeableOperator interface.
//
// Calling:
//
//		obs.Pipe(
//			op1(),
//	 		op2(),
//			op3(),
//		)
//
// is in every way equivalent to:
//
//	op3()(op2()(op1()(obs)))
func (o *Observable[T]) Pipe(ops ...PipeableOperator[T]) IObservable[T] {
	if len(ops) == 0 {
		return o
	}

	newObs := (ops[0]).apply(o).Pipe(ops[1:]...)

	return newObs
}

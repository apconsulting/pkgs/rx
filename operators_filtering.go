package rx

import (
	"math"
	"sync/atomic"
	"time"
)

// DistinctWithEquality emits values only if they are different from the last emitted
// value. Equality is checked with the provided isEqual predicate function.
func DistinctWithEquality[T any](isEqual func(prev T, next T) bool) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription
			var memory T

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					if !isEqual(memory, val) {
						memory = val

						s.Next(val)
					}
				},
				errorFunc: func(err error) {
					s.Error(err)
				},
				completeFunc: func() {
					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

// DistinctUntilChanged is a wrapper around DistinctWithEquality for comparable types
func DistinctUntilChanged[T comparable]() PipeableOperatorFunc[T] {
	return DistinctWithEquality(func(prev, next T) bool {
		return prev == next
	})
}

// Throttle emits and then stays silent for d duration. All discarded values are lost
func Throttle[T any](d time.Duration) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		// If no duration passthrough
		if d == 0 {
			return o
		}

		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription
			var blocked atomic.Bool

			// Can't instantiate a stopped timer. Instead i must create it and immediately
			// stop it before it fires, so i use an appropriately long interval (ty go)
			var timer = time.AfterFunc(math.MaxInt64, func() {
				blocked.Store(false)
			})
			timer.Stop()

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					if !blocked.Load() {
						blocked.Store(true)

						s.Next(val)

						timer.Reset(d)
					}
				},
				errorFunc: func(err error) {
					timer.Stop()
					s.Error(err)
				},
				completeFunc: func() {
					timer.Stop()
					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

// Debounce emits a value from the source Observable only after d duration has passed
// without another source emission.
func Debounce[T any](d time.Duration) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		// If no duration passthrough
		if d == 0 {
			return o
		}

		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription
			var value atomic.Value

			// Can't instantiate a stopped timer. Instead i must create it and immediately
			// stop it before it fires, so i use an appropriately long interval (ty go)
			timer := time.AfterFunc(math.MaxInt64, func() {
				val, ok := value.Load().(T)

				if ok {
					s.Next(val)
				}
			})
			timer.Stop()

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					value.Store(val)
					timer.Reset(d)
				},
				errorFunc: func(err error) {
					timer.Stop()
					s.Error(err)
				},
				completeFunc: func() {
					if timer.Stop() {
						val, ok := value.Load().(T)

						if ok {
							s.Next(val)
						}
					}

					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

// Take emits at most n values before completing.
func Take[T any](n int) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		if n == 0 {
			return Empty[T]()
		}

		return NewObservable(func(s *Subscriber[T]) func() {
			var i int
			var sub *Subscription

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					if i < n {
						s.Next(val)

						i++

						if i == n {
							sub.Unsubscribe()
							s.Complete()
						}
					}
				},
				errorFunc: func(err error) {
					s.Error(err)
				},
				completeFunc: func() {
					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

func Filter[T any](predicate func(val T) bool) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription

			o.operatorSubscribe(NewObserver(
				func(val T) {
					if predicate(val) {
						s.Next(val)
					}
				},
				WithErrorFunc[T](func(err error) {
					s.Error(err)
				}),
				WithCompleteFunc[T](func() {
					s.Complete()
				}),
			), &sub, s)

			return nil
		})
	})
}

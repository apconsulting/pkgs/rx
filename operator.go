package rx

import (
	"math/rand"
	"time"
)

// type PipeableOperator[T any] interface {
// 	Next(val T) (T, bool)
// 	Error(err error) (error, bool)
// 	Complete() bool
// }

// Operator is a generic function interface that accepts an Observable as input and
// returns an Observable as output, which may be of different types
type Operator[T, R any] interface {
	apply(IObservable[T]) IObservable[R]
}

// PipeableOperator interface implemented by all operators that can also be used inside an
// Observable.Pipe() ie. operators where both the input and output observables are of the
// same type
type PipeableOperator[T any] interface {
	Operator[T, T]
}

// Concrete type for Operator
type OperatorFunc[T, R any] func(IObservable[T]) IObservable[R]

func (f OperatorFunc[T, R]) apply(obs IObservable[T]) IObservable[R] {
	return f(obs)
}

// Concrete type for PipeableOperator
type PipeableOperatorFunc[T any] OperatorFunc[T, T]

func (f PipeableOperatorFunc[T]) apply(obs IObservable[T]) IObservable[T] {
	return f(obs)
}

func Double() PipeableOperatorFunc[int] {
	return PipeableOperatorFunc[int](func(o IObservable[int]) IObservable[int] {
		newObs := &Observable[int]{
			onSubscribeFunc: func(s *Subscriber[int]) func() {
				var subscription *Subscription

				o.operatorSubscribe(&Observer[int]{
					nextFunc: func(val int) {
						//fmt.Printf("Doubling %d -> %d\n", val, val*2)
						s.Next(val * 2)
					},
					errorFunc: func(err error) {
						s.Error(err)
					},
					completeFunc: func() {
						s.Complete()
					},
				}, &subscription, s)

				return nil
			},
		}

		return newObs
	})
}

// func OldDelayRandomly[T any]() PipeableOperatorFunc[T] {
// 	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
// 		return &Observable[T]{
// 			onSubscribeFunc: func(s *Subscriber[T]) {
// 				o.Subscribe(&Observer[T]{
// 					nextFunc: func(val T) {
// 						//go func() {
// 						time.Sleep(time.Duration(rand.Intn(3000)) * time.Millisecond)
// 						s.Next(val)
// 						//}()
// 					},
// 					errorFunc: func(err error) {
// 						s.Error(err)
// 					},
// 					completeFunc: func() {
// 						s.Complete()
// 					},
// 				})
// 			},
// 		}

// 		//return newObs
// 	})
// }

func DelayRandomly[T any](ms int) PipeableOperator[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription = NewSubscription()

			type delayedValue struct {
				d time.Duration
				f func()
			}

			ch := make(chan delayedValue, 1000)

			go func() {
				for !s.isDisposed.Load() {
					select {
					case <-s.Disposed:
						return
					case dv, ok := <-ch:
						if !ok {
							return
						}

						select {
						case <-s.Disposed:
							return
						case <-time.After(dv.d): // TODO: VEDERE DI FARLO CON UN TIMER
							dv.f()
						}
					}
				}
			}()

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					// Avoid blocking on full channel
					select {
					case ch <- delayedValue{
						d: time.Duration(rand.Intn(ms)) * time.Millisecond,
						f: func() {
							s.Next(val)
						},
					}:
					default:
					}
				},
				errorFunc: func(err error) {
					s.Error(err)
					close(ch)
				},
				completeFunc: func() {
					// Avoid blocking on full channel
					select {
					case ch <- delayedValue{
						d: time.Duration(rand.Intn(ms)) * time.Millisecond,
						f: func() {
							s.Complete()
						},
					}:
					default:
					}
				},
			}, &sub, s)

			return nil
		})
	})
}

package main

// import (
// 	"fmt"
// 	"math/rand"
// 	"state"
// 	"strconv"
// 	"sync"
// 	"time"
// 	// "github.com/reactivex/rxgo/v2"
// )

// func main() {
// 	var wg sync.WaitGroup

// 	store := state.Store{}
// 	store.Init()

// 	subInt := store.SubToIntValue()
// 	subInt2 := store.SubToIntValue()
// 	subStr := store.SubToStringValue()
// 	subBoth := store.SubToBoth()

// 	intVers := state.StateVersion{}
// 	strVers := state.StateVersion{}
// 	bothVers := state.StateVersion{}

// 	wg.Add(1)
// 	go func() {
// 		var intVal int
// 		for item := range subInt.Obs.Observe() {
// 			V := item.V.(state.ValueVersion)
// 			fmt.Printf("I1 -> Ricevuta notifica in subInt %d / %d\n", V.Value.(int), V.Version.Version)
// 			intVal = V.Value.(int)
// 			intVers = V.Version
// 		}
// 		fmt.Printf("I1 valore finale: %d\n", intVal)
// 		wg.Done()
// 	}()

// 	wg.Add(1)
// 	go func() {
// 		var intVal int
// 		for item := range subInt2.Obs.Observe() {
// 			V := item.V.(state.ValueVersion)
// 			fmt.Printf("I2 -> Ricevuta notifica in subInt %d / %d\n", V.Value.(int), V.Version.Version)
// 			intVal = V.Value.(int)
// 		}
// 		fmt.Printf("I2 valore finale: %d\n", intVal)
// 		wg.Done()
// 	}()

// 	wg.Add(1)
// 	go func() {
// 		var strVal string
// 		for item := range subStr.Obs.Observe() {
// 			V := item.V.(state.ValueVersion)
// 			fmt.Printf("S --> Ricevuta notifica in subStr \"%s\" / %d\n", V.Value.(string), V.Version.Version)
// 			strVal = V.Value.(string)
// 			strVers = V.Version
// 		}
// 		fmt.Printf("S valore finale: \"%s\"\n", strVal)
// 		wg.Done()
// 	}()

// 	wg.Add(1)
// 	go func() {
// 		var intVal int
// 		var strVal string
// 		for item := range subBoth.Obs.Observe() {
// 			V := item.V.(state.ValueVersion)
// 			fmt.Printf("B --> Ricevuta notifica in subBoth %v / %d\n", V.Value.(state.BothStruct), V.Version.Version)
// 			intVal = V.Value.(state.BothStruct).IntValue
// 			strVal = V.Value.(state.BothStruct).StringValue
// 			bothVers = V.Version
// 		}

// 		fmt.Printf("B valore finale: %d / \"%s\"\n", intVal, strVal)
// 		wg.Done()
// 	}()

// 	fmt.Println("Goroutine create...")
// 	time.Sleep(2 * time.Second)

// 	fmt.Println()
// 	fmt.Println("Edito 10 volte int o str a caso")
// 	fmt.Println("-------------------------------")
// 	for i := 0; i < 10; i++ {
// 		// Data race ovunque ma vabbe per sto test lasciamo cosi
// 		if rand.Float64() < 0.5 {
// 			newInt := rand.Intn(100)
// 			fmt.Printf("Edito Int: %d\n", newInt)
// 			if err := store.EditInt(newInt, intVers.Version); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}
// 		} else {
// 			newStr := "Test " + strconv.Itoa(rand.Intn(100))
// 			fmt.Printf("Edito Str: \"%s\"\n", newStr)
// 			if err := store.EditString(newStr, strVers.Version); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}

// 		}

// 		// Speriamo sia piu veloce di cosi
// 		time.Sleep(100 * time.Millisecond)
// 	}

// 	fmt.Println()
// 	fmt.Println("Edit completi")
// 	fmt.Println("-------------")

// 	time.Sleep(time.Second)

// 	fmt.Println()
// 	fmt.Println("Edito Both 3 volte")
// 	fmt.Println("------------------")

// 	for i := 0; i < 3; i++ {
// 		newInt := rand.Intn(100)
// 		newStr := "Test " + strconv.Itoa(rand.Intn(100))
// 		fmt.Printf("Edito both a %d / \"%s\"\n", newInt, newStr)
// 		store.EditBoth(state.BothStruct{
// 			IntValue:    newInt,
// 			StringValue: newStr,
// 		}, bothVers.Version)
// 		time.Sleep(100 * time.Millisecond)
// 	}

// 	fmt.Println()
// 	fmt.Println("Both editato")
// 	fmt.Println("------------")

// 	time.Sleep(time.Second)

// 	fmt.Println()
// 	fmt.Println("Sottoscrivo alla struct")
// 	fmt.Println("-----------------------")

// 	subStruct := store.SubToStruct()
// 	structVers := state.StateVersion{}

// 	wg.Add(1)
// 	go func() {
// 		var structVal state.MyStruct
// 		for item := range subStruct.Obs.Observe() {
// 			V := item.V.(state.ValueVersion)
// 			fmt.Printf("X -> Ricevuta notifica in subStruct %v / %d\n", V.Value.(state.MyStruct), V.Version.Version)
// 			structVal = V.Value.(state.MyStruct)
// 			structVers = V.Version
// 		}
// 		fmt.Printf("X valore finale: \"%v\"\n", structVal)
// 		wg.Done()
// 	}()

// 	time.Sleep(100 * time.Millisecond)

// 	fmt.Println()
// 	fmt.Println("Edito int o str usando la subscription di both")
// 	fmt.Println("----------------------------------------------")

// 	for i := 0; i < 10; i++ {
// 		if rand.Float64() < 0.5 {
// 			newInt := rand.Intn(100)
// 			fmt.Printf("Edito Int: %d\n", newInt)

// 			if err := store.EditIntFromBoth(newInt, bothVers); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}
// 		} else {
// 			newStr := "Test " + strconv.Itoa(rand.Intn(100))
// 			fmt.Printf("Edito Str: \"%s\"\n", newStr)

// 			if err := store.EditStringFromBoth(newStr, bothVers); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}

// 		}

// 		time.Sleep(100 * time.Millisecond)
// 	}

// 	fmt.Println("Finiti edit partendo dal both")
// 	time.Sleep(time.Second)

// 	fmt.Println()
// 	fmt.Println("Edito Int, Str o Struct a caso")
// 	fmt.Println("------------------------------")

// 	for i := 0; i < 20; i++ {
// 		switch rand.Intn(3) {
// 		case 0:
// 			newInt := rand.Intn(100)
// 			fmt.Printf("Edito Int: %d\n", newInt)

// 			if err := store.EditIntFromBoth(newInt, bothVers); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}
// 		case 1:
// 			newStr := "Test " + strconv.Itoa(rand.Intn(100))
// 			fmt.Printf("Edito Str: \"%s\"\n", newStr)

// 			if err := store.EditStringFromBoth(newStr, bothVers); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}
// 		case 2:
// 			newStruct := state.MyStruct{
// 				A: rand.Intn(100),
// 				B: "Test " + strconv.Itoa(rand.Intn(100)),
// 			}
// 			fmt.Printf("Edito Struct: %v\n", newStruct)

// 			if err := store.EditStruct(newStruct, structVers); err != nil {
// 				fmt.Printf("ERRORE: %s\n", err.Error())
// 			}
// 		}

// 		time.Sleep(100 * time.Millisecond)
// 	}

// 	fmt.Println()
// 	fmt.Println("Edit completi")
// 	fmt.Println("-------------")

// 	time.Sleep(time.Second)

// 	fmt.Println("Chiudo lo store")
// 	fmt.Println()

// 	store.Close()

// 	wg.Wait()

// 	time.Sleep(100 * time.Millisecond)
// }

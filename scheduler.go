package rx

// TODO: Incompleto

// L'idea inizialmente è quella di prevedere almeno 3 scheduler diversi:
// - SyncScheduler: esegue l'action in maniera sincrona immediatamente
// - AsyncScheduler: esegue l'action in una propria goroutine
// - DelayedScheduler(d): esegue l'action tramite time.AfterFunc(d, ...)

// Approfondire come funzionano di preciso gli scheduler internamente in RxJS per farsi
// un'idea di come implementare questa cosa

import "time"

type IScheduler interface {
	Execute(f func())
}

type delayedScheduler struct {
	delay time.Duration
}

func (s *delayedScheduler) Execute(f func()) {
	time.AfterFunc(s.delay, f)
}

func DelayedScheduler(d time.Duration) IScheduler {
	return &delayedScheduler{}
}

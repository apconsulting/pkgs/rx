package rx

import (
	"fmt"
	"sync/atomic"
	"time"
)

// TapObserver is a special type of Observer for the Tap operator that registers
// side-effects for various events that Tap listens to.
type ITapObserver[T any] interface {
	IObserver[T]

	OnSubscribe()
	OnUnsubscribe()

	setSubscribeFunc(func())
	setUnsubscribeFunc(func())
}

type TapObserver[T any] struct {
	Observer[T]

	subscribeFunc   func()
	unsubscribeFunc func()
}

func (o *TapObserver[T]) setSubscribeFunc(f func()) {
	o.subscribeFunc = f
}
func (o *TapObserver[T]) setUnsubscribeFunc(f func()) {
	o.unsubscribeFunc = f
}

// TapObserver may have a nil nextFunc, so i have to overload the method and check for it
func (o *TapObserver[T]) Next(val T) {
	if o.nextFunc != nil {
		o.nextFunc(val)
	}
}

func (o *TapObserver[T]) OnSubscribe() {
	if o.subscribeFunc != nil {
		o.subscribeFunc()
	}
}

func (o *TapObserver[T]) OnUnsubscribe() {
	if o.unsubscribeFunc != nil {
		o.unsubscribeFunc()
	}
}

var _ ITapObserver[any] = &TapObserver[any]{}

func TapOnSubscribe[T any](f func()) TapFuncOption[T] {
	return func(o *TapObserver[T]) {
		o.setSubscribeFunc(f)
	}
}
func TapOnUnsubscribe[T any](f func()) TapFuncOption[T] {
	return func(o *TapObserver[T]) {
		o.setUnsubscribeFunc(f)
	}
}
func TapOnNext[T any](f func(val T)) TapFuncOption[T] {
	return func(o *TapObserver[T]) {
		o.setNextFunc(f)
	}
}
func TapOnError[T any](f func(err error)) TapFuncOption[T] {
	return func(o *TapObserver[T]) {
		o.setErrorFunc(f)
	}
}
func TapOnComplete[T any](f func()) TapFuncOption[T] {
	return func(o *TapObserver[T]) {
		o.setCompleteFunc(f)
	}
}

type TapFuncOption[T any] func(*TapObserver[T])

// Tap is used to add side-effect logics to a pipe of operators. Callbacks
// can be set via functional options. Tap is supposed to be used for side-effects
// only (for example logging) and the provided callbacks should never modify
// the received values.
func Tap[T any](opts ...TapFuncOption[T]) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		// If no callbacks are given, passthrough.
		if len(opts) == 0 {
			return o
		}

		tObs := &TapObserver[T]{}

		// Apply all functional options
		for _, opt := range opts {
			opt(tObs)
		}

		return NewObservable(func(s *Subscriber[T]) func() {
			tObs.OnSubscribe() // Notify subscribe

			var sub *Subscription = NewSubscription()

			sub.AddTeardownLogic(tObs.OnUnsubscribe) // Notify unsubscribe

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					tObs.Next(val) // Notify next

					s.Next(val)
				},
				errorFunc: func(err error) {
					tObs.Error(err) // Notify error

					s.Error(err)
				},
				completeFunc: func() {
					tObs.Complete() // Notify complete

					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

type DelayOptions struct {
	chanSize int
}

func WithChannelSize(size int) func(*DelayOptions) {
	return func(opts *DelayOptions) {
		opts.chanSize = size
	}
}

func Delay[T any](d time.Duration, opts ...func(*DelayOptions)) PipeableOperator[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		// if no duration, passthrough
		if d == 0 {
			return o
		}

		options := DelayOptions{
			chanSize: 1000,
		}

		for _, opt := range opts {
			opt(&options)
		}

		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription = NewSubscription()
			var stopped atomic.Bool

			var ErrChannelOverflow error = fmt.Errorf("delay: channel overflow")

			type delayedValue struct {
				d time.Duration
				f func()
			}

			// Channel has a size of 1000. If the upstream observable produces
			// values faster than they are consumed, channel may become full.
			// When this happens, the delay operator errors.
			ch := make(chan delayedValue, options.chanSize)
			done := make(chan struct{})

			// TODO: Questo potrebbe pure essere sincrono, che sia il caso di
			// parametrizzare? Vd. scheduler...
			go func() {
				for !s.isDisposed.Load() && !stopped.Load() {
					select {
					case <-done:
						return
					case <-s.Disposed:
						return
					case dv, ok := <-ch:
						if !ok {
							return
						}

						timer := time.NewTimer(dv.d)

						select {
						case <-s.Disposed:
							timer.Stop()
							return
						case <-timer.C:
							dv.f()
						}
					}
				}
			}()

			var lastEmission time.Time

			o.operatorSubscribe(&Observer[T]{
				nextFunc: func(val T) {
					now := time.Now()
					dur := now.Sub(lastEmission)
					lastEmission = now

					if dur > d {
						dur = d
					}

					// Errors if channel full
					select {
					case ch <- delayedValue{
						d: dur,
						f: func() {
							s.Next(val)
						},
					}:
					default:
						if !stopped.Swap(true) {
							close(done)
							s.Error(ErrChannelOverflow)
							sub.Unsubscribe()
						}
					}
				},
				errorFunc: func(err error) {
					if !stopped.Swap(true) {
						close(done)
						s.Error(err)
						//sub.Unsubscribe()
					}
				},
				completeFunc: func() {
					dur := time.Now().Sub(lastEmission)

					if dur > d {
						dur = d
					}

					// Avoid blocking on full channel
					select {
					case ch <- delayedValue{
						d: dur,
						f: func() {
							s.Complete()
						},
					}:
					default:
						if !stopped.Swap(true) {
							close(done)
							s.Error(ErrChannelOverflow)
							//sub.Unsubscribe()
						}
					}
				},
			}, &sub, s)

			return nil
		})
	})
}

package rx

import (
	"time"
)

// Empty is an observable that does nothing and immediately completes
func Empty[T any]() IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		s.Complete()

		return nil
	})
}

// Never is an observable that does nothing and hangs forever without emitting
func Never[T any]() IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		go func() {
			select {
			case <-s.Disposed:
				return
			}
		}()

		return nil
	})
}

// Throw is an observable that immediately emits the given error
func Throw[T any](err error) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		s.Error(err)

		return nil
	})
}

// Of is an observable that sequentially emits all given values one after another
func Of[T any](vals ...T) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		for _, val := range vals {
			if s.isDisposed.Load() {
				break
			}

			s.Next(val)
		}
		s.Complete()
		return nil
	})
}

// FromSlice turns a slice of values into an observable that emits each one of those
// values sequentially one after another
func FromSlice[T any](values []T) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		for _, val := range values {
			if s.isDisposed.Load() {
				break
			}

			s.Next(val)
		}
		s.Complete()
		return nil
	})
}

// FromMapValues turns all values of the given map into an observable that emits each one
// of those values sequentially one after another. Due to the nature of maps, there is no
// guarantee on the order of emitted values
func FromMapValues[T any](values map[any]T) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		for _, val := range values {
			if s.isDisposed.Load() {
				break
			}

			s.Next(val)
		}
		s.Complete()
		return nil
	})
}

// FromMapKeys turns all keys of the given map into an observable that emits each one of
// those keys sequentially one after another. Due to the nature of maps, there is no
// guarantee on the order of emitted keys
func FromMapKeys[T comparable](values map[T]any) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		for val := range values {
			if s.isDisposed.Load() {
				break
			}

			s.Next(val)
		}
		s.Complete()
		return nil
	})
}

// Range is an observable that sequentially emits m int values starting from n
func Range(n, m int) IObservable[int] {
	return NewObservable(func(s *Subscriber[int]) func() {
		//limit := n + m
		for i := n; i < n+m; i++ {
			if s.isDisposed.Load() {
				break
			}

			//fmt.Printf("Emitting %v\n", n)
			s.Next(i)
		}
		s.Complete()
		return nil
	})
}

// Repeat is an observable that emits val n times.
func Repeat[T any](val T, n int) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		for i := 0; i < n; i++ {
			s.Next(val)
		}
		s.Complete()
		return nil
	})
}

// Timers is an async observable that keeps emitting val every d
func Timer[T any](val T, d time.Duration) IObservable[T] {
	return NewObservable(func(s *Subscriber[T]) func() {
		go func() {
			select {
			case <-s.Disposed:
				return
			case <-time.After(d):
				s.Next(val)
				s.Complete()
			}
		}()
		return nil
	})
}

// Interval emits a new int value every d, starting from 0 and increasing by 1 each time
// it emits
func Interval(d time.Duration) IObservable[int] {
	return NewObservable(func(s *Subscriber[int]) func() {
		go func() {
			var i int

			for {
				select {
				case <-s.Disposed:
					return
				case <-time.After(d):
					s.Next(i)
					i++
				}
			}
		}()
		return nil
	})
}

package rx

// Map applies the project function to all source values before emitting them. It is
// pipeable only if both T and R types of the project function are the same.
//
// Otherwise it must be used outside of the pipe:
//   mappedObs := Map(func(T) R {})(obs)
func Map[T, R any](project func(val T) R) OperatorFunc[T, R] {
	return OperatorFunc[T, R](func(o IObservable[T]) IObservable[R] {
		return NewObservable(func(s *Subscriber[R]) func() {
			var sub *Subscription

			subscribeWithTeardown[T](o, &Observer[T]{
				nextFunc: func(val T) {
					s.Next(project(val))
				},
				errorFunc: func(err error) {
					s.Error(err)
				},
				completeFunc: func() {
					s.Complete()
				},
			}, &sub, s)

			// sub := o.Subscribe(&Observer[T]{
			// 	nextFunc: func(val T) {
			// 		s.Next(project(val))
			// 	},
			// 	errorFunc: func(err error) {
			// 		s.Error(err)
			// 	},
			// 	completeFunc: func() {
			// 		s.Complete()
			// 	},
			// })

			// s.AddTeardownLogic(sub.Unsubscribe)
			//return sub.Unsubscribe
			return nil
		})
	})
}

// Buffers values emitted by a source observable into slices of the given length, then
// emits those slices as single values. This operator is not pipeable.
func Buffer[T any](length int) OperatorFunc[T, []T] {
	return OperatorFunc[T, []T](func(o IObservable[T]) IObservable[[]T] {

		return NewObservable(func(s *Subscriber[[]T]) func() {
			buffer := make([]T, length)
			var i int

			var sub *Subscription

			subscribeWithTeardown[T](o, &Observer[T]{
				nextFunc: func(val T) {
					buffer[i] = val

					if i == length-1 {
						bufCopy := make([]T, length)
						copy(bufCopy, buffer)

						i = 0

						s.Next(bufCopy)
					} else {
						i++
					}
				},
				errorFunc: func(err error) {
					s.Error(err)
				},
				completeFunc: func() {
					if i != 0 {
						s.Next(buffer[0:i])
					}
					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

// Smilar to Buffer but instead of emitting slices of values, it emits observables that,
// in turn, emit those buffered values when subscribed. This operator is not pipeable.
func Window[T any](length int) OperatorFunc[T, IObservable[T]] {
	return OperatorFunc[T, IObservable[T]](func(o IObservable[T]) IObservable[IObservable[T]] {
		return Map(func(val []T) IObservable[T] {
			return FromSlice(val)
		})(
			Buffer[T](3)(o),
		)
	})
}

// Applies an accumulator function to each element emitted by the source observable, then
// stores the returned value of this function and emits it. The accumulator function
// parameters are the last computed value and the element currently being emitted by the
// source observable.
func Scan[T any](accumulator func(x, y T) T) PipeableOperatorFunc[T] {
	return PipeableOperatorFunc[T](func(o IObservable[T]) IObservable[T] {
		var lastVal T
		var first bool = true

		return NewObservable(func(s *Subscriber[T]) func() {
			var sub *Subscription

			subscribeWithTeardown[T](o, &Observer[T]{
				nextFunc: func(val T) {
					if first {
						first = false
						lastVal = val
						s.Next(val)
					} else {
						newVal := accumulator(lastVal, val)
						lastVal = newVal
						s.Next(newVal)
					}
				},
				errorFunc: func(err error) {
					s.Error(err)
				},
				completeFunc: func() {
					s.Complete()
				},
			}, &sub, s)

			return nil
		})
	})
}

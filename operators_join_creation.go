package rx

import (
	"sync"
	"sync/atomic"
)

func ForkJoin[T any](obs ...IObservable[T]) IObservable[[]T] {
	if len(obs) == 0 {
		return Empty[[]T]()
	}

	var buffer map[int]T = make(map[int]T)
	var bufMx sync.Mutex

	subscriptions := make(map[*Subscription]struct{})
	var subsMx sync.Mutex

	var stopped atomic.Bool
	var running atomic.Int32

	running.Store(int32(len(obs)))

	stop := func() {
		if !stopped.Swap(true) {
			subsMx.Lock()
			for sub := range subscriptions {
				if sub != nil {
					sub.Unsubscribe()
				}
			}
			//stopped.Store(true)
			subsMx.Unlock()
		}
	}

	return NewObservable(func(s *Subscriber[[]T]) func() {
		for i, ob := range obs {
			if stopped.Load() {
				break
			}

			// local copy of i
			var index int = i

			newSubscription := NewSubscription()
			subscriptions[newSubscription] = struct{}{}

			newObserver := NewObserver(
				func(val T) {
					bufMx.Lock()
					buffer[index] = val
					bufMx.Unlock()
				},
				WithErrorFunc[T](func(err error) {
					subsMx.Lock()
					delete(subscriptions, newSubscription)
					subsMx.Unlock()

					if !stopped.Swap(true) {
						stop()
						s.Error(err)
					}
				}),
				WithCompleteFunc[T](func() {
					subsMx.Lock()
					delete(subscriptions, newSubscription)
					subsMx.Unlock()

					// "In order for the resulting array to have the same length as the number of
					// input observables, whenever any of the given observables completes without
					// emitting any value, forkJoin will complete at that moment as well and it will
					// not emit anything either, even if it already has some last values from other
					// observables. Conversely, if there is an observable that never completes,
					// forkJoin will never complete either, unless at any point some other
					// observable completes without emitting a value, which brings us back to the
					// previous case. Overall, in order for forkJoin to emit a value, all given
					// observables have to emit something at least once and complete."
					// https://rxjs.dev/api/index/function/forkJoin

					if !stopped.Load() {
						bufMx.Lock()
						_, ok := buffer[index]
						bufMx.Unlock()

						if !ok {
							// Must check stopped again to avoid a race condition
							if !stopped.Swap(true) {
								stop()
								s.Complete()
							}
						} else {
							if running.Add(-1) == 0 {
								// All observables have completed
								result := make([]T, len(buffer))

								for i, v := range buffer {
									result[i] = v
								}

								if len(result) != len(obs) {
									// This should NEVER happen but if it does it means forkJoin is broken,
									// so i panic
									panic("forkJoin: length of result slice != number of input observables")
								}

								s.Next(result)
								s.Complete()
							}
						}
					}
				}),
			)

			subscribeWithTeardown[T](ob,
				newObserver,
				&newSubscription,
				s,
			)
		}

		return nil
	})
}

func Concat[T any](obs ...IObservable[T]) IObservable[T] {
	if len(obs) == 0 {
		return Empty[T]()
	}

	return NewObservable(func(s *Subscriber[T]) func() {
		var i int

		recursiveObserver := &Observer[T]{}
		recursiveObserver.nextFunc = func(val T) {
			s.Next(val)
		}
		recursiveObserver.errorFunc = func(err error) {
			s.Error(err)
		}
		recursiveObserver.completeFunc = func() {
			i++

			if i < len(obs) {
				obs[i].Subscribe(recursiveObserver)
			} else {
				s.Complete()
			}
		}

		obs[0].Subscribe(recursiveObserver)

		return nil
	})
}

// Merge subscribes to all source observables and echoes their values as soon as they are
// emitted. It completes when all source observables complete.
func Merge[T any](obs ...IObservable[T]) IObservable[T] {
	if len(obs) == 0 {
		return Empty[T]()
	}

	return NewObservable(func(s *Subscriber[T]) func() {
		subscriptions := make(map[*Subscription]struct{})
		var subsMx sync.Mutex

		var stopped atomic.Bool

		stop := func() {
			if !stopped.Swap(true) {
				subsMx.Lock()
				for sub := range subscriptions {
					if sub != nil {
						sub.Unsubscribe()
					}
				}
				stopped.Store(true)
				subsMx.Unlock()
			}
		}

		var running atomic.Int32
		running.Store(int32(len(obs)))

		for _, ob := range obs {
			newSub := NewSubscription()

			subsMx.Lock()
			subscriptions[newSub] = struct{}{}
			subsMx.Unlock()

			subscribeWithTeardown[T](ob, NewObserver(
				func(val T) {
					if !stopped.Load() {
						s.Next(val)
					}
				},
				WithErrorFunc[T](func(err error) {
					if !stopped.Swap(true) {
						stop()
						s.Error(err)
					}
				}),
				WithCompleteFunc[T](func() {
					if running.Add(-1) == 0 {
						s.Complete()
					}
				}),
			), &newSub, s)
		}

		return nil
	})
}

// Partition is similar to Filter but instead of creating a single observable of values
// that satisfy the predicate, it also creates an observables of values that don't satisfy
// the predicate
func Partition[T any](ob IObservable[T], predicate func(val T) bool) [2]IObservable[T] {
	var result [2]IObservable[T]

	result[0] = ob.Pipe(
		Filter(predicate),
	)

	result[1] = ob.Pipe(
		Filter(func(val T) bool {
			return !predicate(val)
		}),
	)

	return result
}

// Zip combines multiple Observables to create an Observable whose values are calculated
// from the values, in order, of each of its input Observables Note: all emitted values
// from source observables are buffered, until zip can generate a new value to emit
// itself. Be mindful of potential memory usage implications when combining a fast
// producer with a slow one.
func Zip[T any](obs ...IObservable[T]) IObservable[[]T] {
	if len(obs) == 0 {
		return Empty[[]T]()
	}

	var subscriptions map[*Subscription]struct{} = make(map[*Subscription]struct{})
	var subsMx sync.RWMutex

	type buffer struct {
		values   []T
		complete bool
	}

	var buffers []buffer = make([]buffer, len(obs))
	var bufMx sync.RWMutex

	for i := 0; i < len(obs); i++ {
		buffers[i].values = make([]T, 0)
	}

	var stopped atomic.Bool

	stop := func() {
		if !stopped.Swap(true) {
			subsMx.Lock()
			for sub := range subscriptions {
				if sub != nil {
					sub.Unsubscribe()
				}
			}
			subsMx.Unlock()
		}
	}

	return NewObservable(func(s *Subscriber[[]T]) func() {
		for i, ob := range obs {
			if stopped.Load() {
				break
			}

			var index int = i
			var sub *Subscription = NewSubscription()

			subsMx.Lock()
			subscriptions[sub] = struct{}{}
			subsMx.Unlock()

			bufMx.Lock()
			buffers[index] = buffer{
				values: make([]T, 0),
			}
			bufMx.Unlock()

			newObserver := NewObserver(
				func(val T) {
					// Transactional lock
					bufMx.Lock()
					defer bufMx.Unlock()

					if !stopped.Load() {
						buffers[index].values = append(buffers[index].values, val)

						var ready bool = true
						for _, buf := range buffers {
							if len(buf.values) == 0 {
								ready = false
								break
							}
						}

						if ready {
							var outVal []T = make([]T, len(buffers))
							var done bool

							for i := range buffers {
								outVal[i] = buffers[i].values[0]
								buffers[i].values = buffers[i].values[1:]

								// If any buffer is both complete and has no more values stored, zip
								// completes because no more values could possibly be generated.
								if buffers[i].complete && len(buffers[i].values) == 0 {
									done = true
								}
							}

							s.Next(outVal)

							if done {
								stop()
								s.Complete()
							}
						}
					}
				},
				WithErrorFunc[T](func(err error) {
					if !stopped.Load() {
						stop()
						s.Error(err)
					}
				}),
				WithCompleteFunc[T](func() {
					// Transactional lock
					bufMx.Lock()
					defer bufMx.Unlock()

					buffers[index].complete = true

					// If this source completes and we have no values stored for it waiting to be
					// sent, zip completes because no more values could possibly be generated.
					if len(buffers[index].values) == 0 {
						if !stopped.Load() {
							stop()
							s.Complete()
						}
					}
				}),
			)

			subscribeWithTeardown[T](ob,
				newObserver,
				&sub,
				s,
			)
		}

		return nil
	})
}

package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
	"testing"
	"time"

	"gitlab.com/apconsulting/pkgs/rx"
)

func TestObservable(t *testing.T) {
	syncObs := rx.NewObservable(func(sub *rx.Subscriber[string]) func() {
		sub.Next("Primo")
		sub.Next("Secondo")
		sub.Next("Terzo")
		sub.Complete()

		return nil
	})

	syncObs.Subscribe(TestObserver[string]("1S", nil))
	syncObs.Subscribe(TestObserver[string]("2S", nil))
	syncObs.Subscribe(TestObserver[string]("3S", nil))

	fmt.Println()
	fmt.Println("ASYNC")
	time.Sleep(3 * time.Second)

	var wg sync.WaitGroup
	asyncObs := rx.NewObservable(func(sub *rx.Subscriber[string]) func() {
		sub.Next("Primo")
		sub.Next("Secondo")

		go func() {
			select {
			case <-sub.Disposed:
				sub.Complete()
				return
			case <-time.After(3 * time.Second):
				sub.Next("Terzo")
				sub.Complete()
			}
		}()

		return nil
	})

	wg.Add(3)

	ch1A := asyncObs.Subscribe(TestObserver[string]("1A", &wg))
	asyncObs.Subscribe(TestObserver[string]("2A", &wg))
	asyncObs.Subscribe(TestObserver[string]("3A", &wg))

	fmt.Println("AFTER SUBSCRIBE")

	time.Sleep(time.Second)

	fmt.Println("UNSUBSCRIBING 1A)")
	ch1A.Unsubscribe()
	wg.Done()

	wg.Wait()

	time.Sleep(time.Second)
	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
}

func TestFromChan(t *testing.T) {
	ch := make(chan int)
	done := make(chan struct{})

	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())

	// Emitter
	go func() {
		var i int

		for {
			select {
			case <-done:
				close(ch)
				return
			case <-time.After(time.Duration(rand.Intn(500)) * time.Millisecond):
				i++
				ch <- i
			}
		}
	}()

	fmt.Println("Emitter Started. Waiting 3 seconds...")
	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
	time.Sleep(3 * time.Second)

	obs := rx.SubjectFromChan(ch)

	fmt.Println("Observable created. Waiting 1 second...")
	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
	time.Sleep(time.Second)

	var wg sync.WaitGroup
	wg.Add(3)

	ch1A := obs.Subscribe(TestObserver[int]("1", &wg))
	obs.Subscribe(TestObserver[int]("2", &wg))
	obs.Subscribe(TestObserver[int]("3", &wg))

	fmt.Println("Subscriptions made")
	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
	fmt.Println("Waiting 5 seconds...")
	time.Sleep(5 * time.Second)

	fmt.Println("Unsubscribing 1)")
	ch1A.Unsubscribe()
	wg.Done()

	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
	fmt.Println("Waiting 3 seconds...")
	time.Sleep(3 * time.Second)

	fmt.Println("Stopping emitter")
	close(done)
	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
	fmt.Println("Emitter stopped. Waiting on WaitGroup...")

	wg.Wait()

	fmt.Println("Waiting for everything to shut down...")
	time.Sleep(time.Second)

	fmt.Printf("Goroutines: %d\n", runtime.NumGoroutine())
}

func TestSubject(t *testing.T) {
	var wg sync.WaitGroup
	asyncObs := rx.NewObservable(func(sub *rx.Subscriber[string]) func() {
		sub.Next("Primo")
		sub.Next("Secondo")

		go func() {
			select {
			case <-sub.Disposed:
				return
			case <-time.After(3 * time.Second):
				sub.Next("Terzo")
				sub.Complete()
			}
		}()

		return nil
	})

	fmt.Println("Observable created")

	subj := rx.NewSubject[string]()

	fmt.Println("Subject created")
	time.Sleep(time.Second)

	wg.Add(3)
	subscription1 := subj.Subscribe(TestObserver[string]("1", &wg))
	subj.Subscribe(TestObserver[string]("2", &wg))
	subj.Subscribe(TestObserver[string]("3", &wg))

	fmt.Println("Observers connected to Subject")

	time.Sleep(time.Second)

	fmt.Println("Subscribing Subject to Observable")
	asyncObs.Subscribe(subj)
	fmt.Println("After subject subscription")

	time.Sleep(time.Second)

	fmt.Println("Unsubscribing 1) and subscribing 4) to subject")
	subscription1.Unsubscribe()
	wg.Done()
	wg.Add(1)
	subj.Subscribe(TestObserver[string]("4", &wg))
	fmt.Println("Done!")

	wg.Wait()

	fmt.Println("Subscribing after subject is completed")
	wg.Add(1)
	subj.Pipe(
		TestTapping[string]("5", true),
	).Subscribe(TestObserver[string]("5", &wg))

	wg.Wait()
}

func TestBehaviorSubject(t *testing.T) {
	var wg sync.WaitGroup
	var ch chan int = make(chan int)

	fmt.Println("START")

	subj := rx.BehaviorSubjectFromChan(99, ch)

	wg.Add(1)
	subj.Subscribe(TestObserver[int]("O0", &wg))

	go func() {
		rx.Interval(time.Second).Pipe(
			TestTapping[int]("Interval", true),
		).Subscribe(rx.NewObserver(
			func(val int) {
				// Non-blocking send
				select {
				case ch <- val:
				default:
				}
			},
		))
	}()

	fmt.Println("Emitter started")

	time.Sleep(3500 * time.Millisecond)

	fmt.Println("Subscribing O1")
	wg.Add(1)
	subj.Subscribe(TestObserver[int]("O1", &wg))

	time.Sleep(3 * time.Second)

	fmt.Println("Subscribing O2")
	wg.Add(1)
	subj.Subscribe(TestObserver[int]("O2", &wg))

	time.Sleep(5 * time.Second)

	close(ch)

	wg.Wait()
}

func TestReplaySubject(t *testing.T) {
	var wg sync.WaitGroup
	var ch chan int = make(chan int)

	fmt.Println("START")

	subj := rx.ReplaySubjectFromChan(5, ch)

	wg.Add(1)
	subj.Pipe(
		TestTapping[int]("O0", false),
	).Subscribe(TestObserver[int]("O0", &wg))

	go func() {
		rx.Interval(time.Second).Pipe(
			TestTapping[int]("Interval", true),
		).Subscribe(rx.NewObserver(
			func(val int) {
				// Non-blocking send
				select {
				case ch <- val:
				default:
				}
			},
		))
	}()

	fmt.Println("Emitter started")

	time.Sleep(1500 * time.Millisecond)

	fmt.Println("Subscribing O1")
	wg.Add(1)
	subj.Pipe(
		TestTapping[int]("O1", false),
	).Subscribe(TestObserver[int]("O1", &wg))

	time.Sleep(time.Second)

	fmt.Println("Subscribing O2")
	wg.Add(1)
	subj.Pipe(
		TestTapping[int]("O2", false),
	).Subscribe(TestObserver[int]("O2", &wg))

	time.Sleep(5 * time.Second)

	fmt.Println("Subscribing O3")
	wg.Add(1)
	subj.Pipe(
		TestTapping[int]("O3", false),
	).Subscribe(TestObserver[int]("O3", &wg))

	time.Sleep(5 * time.Second)

	close(ch)

	wg.Wait()
}

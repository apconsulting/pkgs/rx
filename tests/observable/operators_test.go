package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"strconv"
	"sync"
	"testing"
	"time"

	"gitlab.com/apconsulting/pkgs/rx"
)

func TestPipe(t *testing.T) {
	var wg sync.WaitGroup

	outObs := rx.NewObservable(func(s *rx.Subscriber[int]) func() {
		fmt.Println("outObs producer started")
		s.Next(1)
		s.Next(2)
		s.Next(3)
		go func() {
			time.Sleep(time.Second)
			s.Next(4)
			s.Next(5)

			time.Sleep(10 * time.Second)

			s.Complete()
		}()

		return nil
	})

	inObs := rx.Map(func(val int) string {
		return strconv.Itoa(val) + " come Stringa"
	})(
		outObs.Pipe(
			rx.Double(),
			rx.DelayRandomly[int](1000),
			rx.Map(func(val int) int {
				return val
			}),
		),
	)

	fmt.Println("Observables creted")
	time.Sleep(time.Second)

	fmt.Println("Subscribing to inObs...")
	wg.Add(2)

	inObs.Subscribe(TestObserver[string]("1", &wg))
	inObs.Subscribe(TestObserver[string]("2", &wg))

	fmt.Println("Just after subscription")

	wg.Wait()
}

func TestSubjectPipe(t *testing.T) {
	var wg sync.WaitGroup
	ch := make(chan int)
	subj := rx.SubjectFromChan(ch)

	fmt.Println("Subject created")

	wg.Add(2)
	subscription1 := subj.Subscribe(TestObserver[int]("1", &wg))
	subj.Subscribe(TestObserver[int]("2", &wg))

	fmt.Println("Observers subscribed")
	time.Sleep(time.Second)

	fmt.Println("Creating pipe on Subject")

	piped := subj.Pipe(
		rx.Double(),
	)

	fmt.Println("Pipe created")
	time.Sleep(time.Second)

	fmt.Println("Subscribing to piped subject")
	wg.Add(1)
	piped.Subscribe(TestObserver[int]("P", &wg))

	fmt.Println("Subscribed to pipe")
	time.Sleep(time.Second)

	fmt.Println("Starting emitter")

	go func() {
		for i := 0; i < 10; i++ {
			fmt.Printf("Emitting %v\n", i)

			ch <- i

			time.Sleep(time.Second)
		}

		close(ch)
	}()

	time.Sleep(5 * time.Second)

	fmt.Println("Unsubscribing 1)")
	subscription1.Unsubscribe()
	wg.Done()

	wg.Wait()
}

func TestDistinct(t *testing.T) {
	rx.Interval(time.Second).Pipe(
		rx.Take[int](10),
		rx.Tap(
			rx.TapOnNext(func(val int) {
				fmt.Printf("Tapped %v\n", val)
			}),
		),
		rx.DistinctUntilChanged[int](),
	).Subscribe(TestObserver[int]("1", nil))
}

func TestBufferSubject(t *testing.T) {
	subj := rx.NewSubject[int]()

	buffered := rx.Buffer[int](3)(subj)

	subj.Subscribe(TestObserver[int]("SUBJ", nil))
	buffered.Subscribe(TestObserver[[]int]("BUFF", nil))

	fmt.Println("Subscribing subject to interval")

	rx.Range(0, 10).Subscribe(subj)

	fmt.Println("Unsubscribed from Interval")
}

func TestBuffer(t *testing.T) {
	//var wg sync.WaitGroup

	//wg.Add(1)
	subscription := rx.Buffer[int](3)(
		rx.Interval(time.Second/4).Pipe(
			TestTapping[int]("Interval", true),
			rx.Take[int](15),
		),
	).Pipe(
		TestTapping[[]int]("Buffer", false),
	).Subscribe(TestObserver[[]int]("BUF", nil))

	time.Sleep(3 * time.Second)
	fmt.Println("UNSUBSCRIBING FROM BUFFER")
	subscription.Unsubscribe()

	time.Sleep(time.Second)

	//wg.Wait()
}

func TestScan(t *testing.T) {
	rx.Range(1, 10).Pipe(
		rx.Scan(func(x, y int) int {
			return x + y
		}),
	).Subscribe(TestObserver[int]("A", nil))

	rx.Of("a", "b", "c", "d", "e", "f").Pipe(
		rx.Scan(func(x, y string) string {
			return x + y
		}),
	).Subscribe(TestObserver[string]("B", nil))
}

func TestWindow(t *testing.T) {
	subj := rx.NewSubject[int]()
	windowed := rx.Window[int](3)(subj)
	var i int

	subj.Subscribe(TestObserver[int]("SUBJ", nil))

	windowed.Subscribe(rx.NewObserver(
		func(inObs rx.IObservable[int]) {
			fmt.Println("Received observable... Subscribing")
			inObs.Subscribe(TestObserver[int]("OBS", nil))
			i++
		},
	))

	fmt.Println("Subscribing subject to Emitter")

	subscription := rx.Of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10).Subscribe(subj)

	subscription.Unsubscribe()

	fmt.Println("Unsubscribed from Interval")
}

func TestTake(t *testing.T) {
	fmt.Println("BEFORE")

	rx.Of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10).Pipe(
		rx.Double(),
		rx.Take[int](5),
		rx.Double(),
	).Subscribe(rx.NewObserver(
		func(val int) {
			fmt.Printf("NEXTED %v\n", val)
		},
	))
	fmt.Println("AFTER")

	time.Sleep(time.Second)
}

func TestTap(t *testing.T) {
	fmt.Println("START")

	rx.Range(0, 10).Pipe(
		rx.Tap(
			rx.TapOnNext(func(val int) {
				fmt.Printf("Tapped %v\n", val)
			}),
			rx.TapOnSubscribe[int](func() {
				fmt.Println("Tapped Subscribe")
			}),
			rx.TapOnUnsubscribe[int](func() {
				fmt.Println("Tapped Unsubscribe")
			}),
		),
		rx.Double(),
		rx.Take[int](5),
	).Subscribe(TestObserver[int]("S", nil))

	fmt.Println("AFTER SUBSCRIPTIONS")
}

func TestDelay(t *testing.T) {
	var wg sync.WaitGroup
	wg.Add(1)

	fmt.Printf("Goroutines: %v\n", runtime.NumGoroutine())

	rx.Range(0, 50).Pipe(
		rx.DelayRandomly[int](300),
		//rx.Take[int](25),
		rx.Tap(
			rx.TapOnNext(func(val int) {
				fmt.Printf("Tapped %v\n", val)
			}),
		),
		rx.Delay[int](500*time.Millisecond, rx.WithChannelSize(5)),
	).Subscribe(TestObserver[int]("SUB", &wg))

	fmt.Println("AFTER SUBSCRIPTIONS")
	fmt.Printf("Goroutines: %v\n", runtime.NumGoroutine())

	time.Sleep(250 * time.Millisecond)
	fmt.Printf("Goroutines: %v\n", runtime.NumGoroutine())

	wg.Wait()
	fmt.Println("AFTER WAITING")
	fmt.Printf("Goroutines: %v\n", runtime.NumGoroutine())
	time.Sleep(time.Second)
	fmt.Printf("Goroutines: %v\n", runtime.NumGoroutine())
	time.Sleep(time.Second)
}

func TestForkJoin(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("-------- BEFORE SUBSCRIPTION")
	wg.Add(1)

	subj := rx.NewSubject[int]()

	rx.ForkJoin(
		rx.Range(0, 10).Pipe(
			TestTapping[int]("RANGE", false),
		),

		subj.Pipe(
			TestTapping[int]("SUBJ", true),
		),

		rx.Interval(500*time.Millisecond).Pipe(
			rx.Take[int](3),
			TestTapping[int]("INTERVAL", true),
		),

		rx.Of(2, 4, 6).Pipe(
			TestTapping[int]("OF", false),
		),

		rx.NewObservable(func(s *rx.Subscriber[int]) func() {
			go func() {
				time.Sleep(850 * time.Millisecond)
				//s.Error(error(nil))
				s.Next(23)
				s.Complete()
			}()

			return nil
		}).Pipe(
			TestTapping[int]("ASYNCOBS", false),
		),

		rx.NewObservable(func(s *rx.Subscriber[int]) func() {
			s.Next(1)
			s.Complete()

			return nil
		}).Pipe(
			TestTapping[int]("NEWOBS", false),
		),
	).Subscribe(TestObserver[[]int]("FJ", &wg))

	fmt.Println("-------- AFTER SUBSCRIPTION")

	go func() {
		time.Sleep(500 * time.Millisecond)
		subj.Next(100)
		time.Sleep(500 * time.Millisecond)
		subj.Next(200)
		subj.Complete()
	}()

	wg.Wait()

	time.Sleep(100 * time.Millisecond)
}

func TestConcat(t *testing.T) {
	fmt.Println("----- BEFORE SUBSCRIBING")
	var wg sync.WaitGroup

	wg.Add(1)
	rx.Concat(
		rx.Range(0, 10).Pipe(
			TestTapping[int]("RANGE", false),
		),
		rx.NewObservable(func(s *rx.Subscriber[int]) func() {
			s.Next(100)
			s.Next(200)
			go func() {
				time.Sleep(time.Second)
				s.Next(300)
				time.Sleep(time.Second)
				//s.Error(error(nil))
				s.Complete()
			}()

			return nil
		}),
		rx.Interval(time.Second).Pipe(
			rx.Take[int](3),
			TestTapping[int]("INTERVAL", true),
		),
	).Subscribe(TestObserver[int]("OUT", &wg))

	fmt.Println("----- AFTER SUBSCRIBING")

	wg.Wait()
}

func TestMerge(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("-------- BEFORE SUBSCRIPTION")
	wg.Add(1)

	subj := rx.NewSubject[int]()

	rx.Merge(
		rx.Range(0, 10).Pipe(
			TestTapping[int]("RANGE", true),
		),

		subj.Pipe(
			TestTapping[int]("SUBJ", true),
		),

		rx.Interval(500*time.Millisecond).Pipe(
			rx.Take[int](3),
			TestTapping[int]("INTERVAL", true),
		),

		rx.Of(2, 4, 6).Pipe(
			TestTapping[int]("OF", true),
		),

		rx.NewObservable(func(s *rx.Subscriber[int]) func() {
			go func() {
				time.Sleep(850 * time.Millisecond)
				//s.Error(error(nil))
				s.Next(23)
				s.Complete()
			}()

			return nil
		}).Pipe(
			TestTapping[int]("ASYNCOBS", true),
		),

		rx.NewObservable(func(s *rx.Subscriber[int]) func() {
			s.Next(1)
			s.Complete()

			return nil
		}).Pipe(
			TestTapping[int]("NEWOBS", true),
		),
	).Subscribe(TestObserver[int]("MERGE", &wg))

	fmt.Println("-------- AFTER SUBSCRIPTION")

	go func() {
		time.Sleep(500 * time.Millisecond)
		subj.Next(100)
		time.Sleep(500 * time.Millisecond)
		subj.Next(200)
		subj.Complete()
	}()

	wg.Wait()

	time.Sleep(100 * time.Millisecond)
}

func TestFilter(t *testing.T) {
	rx.Range(0, 10).Pipe(
		rx.Filter(func(val int) bool {
			return val%2 == 0
		}),
	).Subscribe(TestObserver[int]("SUB", nil))
}

func TestPartition(t *testing.T) {
	var wg sync.WaitGroup

	wg.Add(2)
	fmt.Println("START")
	obs := rx.Partition(
		rx.Interval(time.Second/4).Pipe(
			rx.Take[int](20),
		),
		func(val int) bool {
			return val%2 == 0
		},
	)

	obs[0].Subscribe(TestObserver[int]("0", &wg))
	time.Sleep(3 * time.Second)
	obs[1].Subscribe(TestObserver[int]("1", &wg))
	fmt.Println("AFTER SUBSCRIPTIONS")

	wg.Wait()
}

func TestZip(t *testing.T) {
	var wg sync.WaitGroup

	wg.Add(1)

	fmt.Println("START")
	rx.Zip(
		rx.Range(0, 10).Pipe(
			TestTapping[int]("Tap1", false),
		),
		rx.Range(10, 15).Pipe(
			rx.DelayRandomly[int](1000),
			TestTapping[int]("Tap2", false),
		),
		rx.Range(20, 10).Pipe(
			rx.DelayRandomly[int](500),
			TestTapping[int]("Tap3", false),
		),
	).Subscribe(TestObserver[[]int]("ZIP", &wg))
	fmt.Println("AFTER SUBSCRIBE")

	wg.Wait()
}

func TestCatchError(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("START")
	wg.Add(1)
	rx.NewObservable(func(s *rx.Subscriber[int]) func() {
		s.Next(1)
		s.Next(2)
		s.Next(99999)
		go func() {
			time.Sleep(250 * time.Millisecond)
			if rand.Intn(100) < 85 {
				s.Error(fmt.Errorf("testing"))
			} else {
				s.Complete()
			}
		}()

		return nil
	}).Pipe(
		TestTapping[int]("TAP", false),
		rx.CatchError(func(err error, i rx.IObservable[int]) rx.IObservable[int] {
			return i
		}),
	).Subscribe(TestObserver[int]("OUT", &wg))
	fmt.Println("AFTER")

	wg.Wait()
}

func TestThrottle(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("START")

	wg.Add(1)
	rx.Interval(300*time.Millisecond).Pipe(
		rx.Throttle[int](time.Second),
		rx.Take[int](8),
	).Subscribe(TestObserver[int]("OBS", &wg))

	fmt.Println("AFTER")

	wg.Wait()
}

func TestDebounce(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("START")

	wg.Add(1)
	rx.NewObservable(func(s *rx.Subscriber[int]) func() {
		s.Next(0)
		time.Sleep(500 * time.Millisecond)
		s.Next(1)
		time.Sleep(500 * time.Millisecond)
		s.Next(2)
		time.Sleep(500 * time.Millisecond)
		s.Next(3)
		time.Sleep(time.Second)
		s.Next(4)
		time.Sleep(time.Second)
		s.Next(5)
		time.Sleep(time.Second)
		s.Next(6)
		s.Complete()

		return nil
	}).Pipe(
		TestTapping[int]("OBS", true),
		rx.Debounce[int](800*time.Millisecond),
	).Subscribe(TestObserver[int]("OBS", &wg))

	fmt.Println("AFTER")

	wg.Wait()
}

func TestMap(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("START")

	wg.Add(1)
	rx.Map(func(i int) string {
		return "Stringa: " + strconv.Itoa(i)
	})(
		rx.Interval(time.Second).Pipe(
			TestTapping[int]("Interval", true),
		),
	).Pipe(
		rx.Take[string](5),
	).Subscribe(TestObserver[string]("OBS", &wg))

	fmt.Println("AFTER")

	wg.Wait()

	time.Sleep(3 * time.Second)
}

func TestOnSubsc(t *testing.T) {
	var wg sync.WaitGroup

	fmt.Println("START")

	wg.Add(1)
	sub := rx.NewObservable(func(s *rx.Subscriber[int]) func() {
		s.Next(1)
		s.Next(2)
		s.Next(3)

		go func() {
			time.Sleep(time.Second)
			s.Next(4)
			time.Sleep(time.Second)
			s.Next(5)
			time.Sleep(time.Second)
			s.Next(6)
			time.Sleep(time.Second)
			s.Next(7)
			s.Complete()
		}()

		return func() {
			fmt.Println("TD FUNC")
		}
	}).Subscribe(TestObserver[int]("OBS", &wg))

	time.Sleep(2 * time.Second)

	sub.Unsubscribe()

	wg.Wait()

	time.Sleep(time.Second)
}

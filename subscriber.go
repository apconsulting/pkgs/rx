package rx

// Subscriber implements the IObserver interface and extends Subscription
type Subscriber[T any] struct {
	obs IObserver[T]

	*Subscription
}

// Assert implementation
var _ IObserver[any] = &Subscriber[any]{}

func (s *Subscriber[T]) Next(val T) {
	if !s.isDisposed.Load() {
		s.obs.Next(val)
	}
}
func (s *Subscriber[T]) Error(err error) {
	if !s.isDisposed.Swap(true) {
		s.obs.Error(err)
	}
}
func (s *Subscriber[T]) Complete() {
	if !s.isDisposed.Swap(true) {
		s.obs.Complete()
	}
}

package rx

// import (
// 	"context"
// 	"fmt"
// 	"sync"

// 	"github.com/reactivex/rxgo/v2"
// )

// var (
// 	ErrVersionMismatch = fmt.Errorf("version mismatch")
// )

// type MyStruct struct {
// 	A int
// 	B string
// }

// type StoreValue struct {
// 	IntValue    int
// 	StringValue string
// 	StructValue MyStruct

// 	SliceValue []string
// 	MapValue   map[int]struct{}
// }

// func (g *StoreValue) Init() {
// 	g.SliceValue = make([]string, 0)
// 	g.MapValue = make(map[int]struct{})
// }

// type Subscription struct {
// 	Obs rxgo.Observable
// 	key string
// }

// type Projection struct {
// 	Obs rxgo.Observable
// }

// type ValueVersion struct {
// 	Version StateVersion
// 	Value   any
// }

// type StateVersion struct {
// 	Version int
// 	key     string
// }

// type BothStruct struct {
// 	IntValue    int
// 	StringValue string
// }

// type Store struct {
// 	trLock sync.Mutex

// 	obsMap      map[string]State
// 	obsMapMutex sync.RWMutex

// 	// vers int
// 	value           StoreValue
// 	storeObservable rxgo.Observable

// 	storeChan chan rxgo.Item
// }

// func (s *Store) Init() {
// 	s.value.Init()
// 	s.storeChan = make(chan rxgo.Item)

// 	s.obsMap = make(map[string]State)

// 	s.storeObservable = rxgo.FromEventSource(s.storeChan)

// 	// Inizializzo tutto
// 	s.storeChan <- rxgo.Of(s.value)
// }

// func (s *Store) SubToIntValue() Subscription {
// 	const mapKey = "SubToIntValue"

// 	s.obsMapMutex.Lock()
// 	state, ok := s.obsMap[mapKey]

// 	if !ok {
// 		// obs := s.storeObservable.Map(func(_ context.Context, i any) (any, error) {
// 		// 	return i.(StoreValue).IntValue, nil
// 		// }).DistinctUntilChanged(func(_ context.Context, i any) (any, error) {
// 		// 	return i, nil
// 		// })

// 		// obs.DoOnNext(func(i interface{}) {
// 		// 	s.obsMapMutex.Lock()
// 		// 	defer s.obsMapMutex.Unlock()

// 		// 	fmt.Println("SIDE EFFECT")

// 		// 	obs, ok := s.obsMap[mapKey]
// 		// 	if !ok {
// 		// 		panic(fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey))
// 		// 	}

// 		// 	obs.version++
// 		// 	s.obsMap[mapKey] = obs
// 		// })

// 		state = State{
// 			// obs: obs.Map(func(ctx context.Context, i any) (any, error) {
// 			// 	s.obsMapMutex.RLock()
// 			// 	defer s.obsMapMutex.RUnlock()

// 			// 	obs, ok := s.obsMap[mapKey]

// 			// 	if !ok {
// 			// 		panic(fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey))
// 			// 	}

// 			// 	return ValueVersion{
// 			// 		Version: StateVersion{
// 			// 			Version: obs.version,
// 			// 			key:     mapKey,
// 			// 		},
// 			// 		Value: i,
// 			// 	}, nil
// 			// }),
// 			obs: rxgo.FromEventSource(s.storeObservable.Map(func(_ context.Context, i any) (any, error) {
// 				return i.(StoreValue).IntValue, nil
// 			}).DistinctUntilChanged(func(_ context.Context, i any) (any, error) {
// 				return i, nil
// 			}).Map(func(_ context.Context, i any) (any, error) {
// 				s.obsMapMutex.Lock()
// 				defer s.obsMapMutex.Unlock()

// 				obs, ok := s.obsMap[mapKey]
// 				if !ok {
// 					panic(fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey))
// 				}

// 				obs.version++
// 				s.obsMap[mapKey] = obs

// 				return ValueVersion{
// 					Version: StateVersion{
// 						Version: obs.version,
// 						key:     mapKey,
// 					},
// 					Value: i,
// 				}, nil
// 			}).Observe()),
// 			version: 0,
// 		}

// 		s.obsMap[mapKey] = state
// 	}
// 	s.obsMapMutex.Unlock()

// 	return Subscription{
// 		Obs: state.obs,
// 		key: mapKey,
// 	}
// }

// func (s *Store) SubToStringValue() Subscription {
// 	const mapKey = "SubToStringValue"

// 	s.obsMapMutex.Lock()
// 	state, ok := s.obsMap[mapKey]

// 	if !ok {
// 		state = State{
// 			obs: rxgo.FromEventSource(s.storeObservable.Map(func(_ context.Context, i any) (any, error) {
// 				return i.(StoreValue).StringValue, nil
// 			}).DistinctUntilChanged(func(_ context.Context, i any) (any, error) {
// 				return i, nil
// 			}).Map(func(_ context.Context, i any) (any, error) {
// 				s.obsMapMutex.Lock()
// 				defer s.obsMapMutex.Unlock()

// 				obs, ok := s.obsMap[mapKey]
// 				if !ok {
// 					panic(fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey))
// 				}

// 				obs.version++
// 				s.obsMap[mapKey] = obs

// 				return ValueVersion{
// 					Version: StateVersion{
// 						Version: obs.version,
// 						key:     mapKey,
// 					},
// 					Value: i,
// 				}, nil
// 			}).Observe()),
// 			version: 0,
// 		}

// 		s.obsMap[mapKey] = state
// 	}
// 	s.obsMapMutex.Unlock()

// 	return Subscription{
// 		Obs: state.obs,
// 		key: mapKey,
// 	}
// }

// func (s *Store) SubToBoth() Subscription {
// 	const mapKey = "SubToBoth"

// 	s.obsMapMutex.Lock()
// 	state, ok := s.obsMap[mapKey]

// 	if !ok {
// 		state = State{
// 			obs: rxgo.FromEventSource(s.storeObservable.Map(func(_ context.Context, i any) (any, error) {
// 				st := i.(StoreValue)

// 				return BothStruct{
// 					IntValue:    st.IntValue,
// 					StringValue: st.StringValue,
// 				}, nil
// 			}).DistinctUntilChanged(func(_ context.Context, i any) (any, error) {
// 				return i, nil
// 			}).Map(func(_ context.Context, i any) (any, error) {
// 				s.obsMapMutex.Lock()
// 				defer s.obsMapMutex.Unlock()

// 				obs, ok := s.obsMap[mapKey]
// 				if !ok {
// 					return nil, fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey)
// 				}

// 				obs.version++
// 				s.obsMap[mapKey] = obs

// 				return ValueVersion{
// 					Version: StateVersion{
// 						Version: obs.version,
// 						key:     mapKey,
// 					},
// 					Value: i,
// 				}, nil
// 			}).Observe()),
// 			version: 0,
// 		}

// 		s.obsMap[mapKey] = state
// 	}
// 	s.obsMapMutex.Unlock()

// 	return Subscription{
// 		Obs: state.obs,
// 		key: mapKey,
// 	}
// }

// func (s *Store) SubToStruct() Subscription {
// 	const mapKey = "SubToStruct"

// 	s.obsMapMutex.Lock()
// 	state, ok := s.obsMap[mapKey]

// 	if !ok {
// 		state = State{
// 			obs: rxgo.FromEventSource(s.storeObservable.Map(func(_ context.Context, i any) (any, error) {
// 				st := i.(StoreValue)

// 				return st.StructValue, nil
// 			}).DistinctUntilChanged(func(_ context.Context, i any) (any, error) {
// 				return i, nil
// 			}).Map(func(_ context.Context, i any) (any, error) {
// 				s.obsMapMutex.Lock()
// 				defer s.obsMapMutex.Unlock()

// 				obs, ok := s.obsMap[mapKey]
// 				if !ok {
// 					return nil, fmt.Errorf("Non riesco a trovare %s nella mappa degli observable!", mapKey)
// 				}

// 				obs.version++
// 				s.obsMap[mapKey] = obs

// 				return ValueVersion{
// 					Version: StateVersion{
// 						Version: obs.version,
// 						key:     mapKey,
// 					},
// 					Value: i,
// 				}, nil
// 			}).Observe()),
// 			version: 0,
// 		}

// 		s.obsMap[mapKey] = state
// 	}
// 	s.obsMapMutex.Unlock()

// 	return Subscription{
// 		Obs: state.obs,
// 		key: mapKey,
// 	}
// }

// func (s *Store) Close() {
// 	close(s.storeChan)
// }

// func (s *Store) EditInt(i int, v int) error {
// 	const mapKey = "SubToIntValue"

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v != obs.version {
// 		return fmt.Errorf("%s: got %d wanted %d", ErrVersionMismatch.Error(), v, obs.version)
// 	}

// 	s.value.IntValue = i

// 	s.storeChan <- rxgo.Of(s.value)

// 	return nil
// }

// func (s *Store) EditString(i string, v int) error {
// 	const mapKey = "SubToStringValue"

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v != obs.version {
// 		return ErrVersionMismatch
// 	}

// 	s.value.StringValue = i
// 	s.storeChan <- rxgo.Item{
// 		V: s.value,
// 	}

// 	return nil
// }

// // Questo metodo non dovrebbe esistere, dovrei fornire solo EditInt e EditString che accettando come versione
// // l'intero oggetto Subscription, così posso recuperare l'obs corretto dalla mappa e verificare la versione
// func (s *Store) EditBoth(i BothStruct, v int) error {
// 	const mapKey = "SubToBoth"

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v != obs.version {
// 		return ErrVersionMismatch
// 	}

// 	s.value.IntValue = i.IntValue
// 	s.value.StringValue = i.StringValue

// 	s.storeChan <- rxgo.Of(s.value)

// 	return nil
// }

// // I prossimi 2 metodi diventeranno EditInt e EditString, queste versioni sono di test
// func (s *Store) EditIntFromBoth(i int, v StateVersion) error {
// 	var mapKey = v.key

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v.Version != obs.version {
// 		return fmt.Errorf("%s: got %d wanted %d", ErrVersionMismatch.Error(), v.Version, obs.version)
// 	}

// 	s.value.IntValue = i
// 	s.storeChan <- rxgo.Item{
// 		V: s.value,
// 	}

// 	return nil
// }

// func (s *Store) EditStringFromBoth(i string, v StateVersion) error {
// 	var mapKey = v.key

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v.Version != obs.version {
// 		return ErrVersionMismatch
// 	}

// 	s.value.StringValue = i
// 	s.storeChan <- rxgo.Of(s.value)

// 	return nil
// }

// func (s *Store) EditStruct(i MyStruct, v StateVersion) error {
// 	var mapKey = v.key

// 	s.obsMapMutex.Lock()
// 	obs, ok := s.obsMap[mapKey]
// 	s.obsMapMutex.Unlock()

// 	if !ok {
// 		return fmt.Errorf("Observable not found")
// 	}

// 	if v.Version != obs.version {
// 		return ErrVersionMismatch
// 	}

// 	s.value.StructValue = i
// 	s.storeChan <- rxgo.Of(s.value)

// 	return nil
// }

package main

import (
	"context"
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/reactivex/rxgo/v2"
)

type ValueVersion struct {
	Val     any
	Version int
}

func main() {
	var wg sync.WaitGroup
	ch := make(chan rxgo.Item)
	var count int

	obs2 := rxgo.FromEventSource(ch) /* .Map(func(ctx context.Context, i interface{}) (interface{}, error) {
		return ValueVersion{
			Val:     i,
			Version: count,
		}, nil
	}) */

	obs2.DoOnNext(func(i interface{}) {
		count++
		fmt.Printf("Porto count a %d\n", count)
	})

	//obs2 := rxgo.FromEventSource(obs.Observe())

	ctx4, canc4 := context.WithCancel(context.Background())
	ctx5, canc5 := context.WithCancel(context.Background())

	for i := 1; i <= 3; i++ {
		wg.Add(1)
		go func(i int) {
			for item := range obs2.Observe() {
				v := item.V.(ValueVersion)
				fmt.Printf("%d) Ricevuto %d / %d\n", i, v.Val, v.Version)
			}
			fmt.Printf("%d) CLOSED\n", i)
			wg.Done()
		}(i)
	}

	wg.Add(1)
	go func(i int) {
		<-obs2.DoOnNext(func(item interface{}) {
			v := item.(ValueVersion)
			fmt.Printf("%d) Ricevuto %d / %d\n", i, v.Val, v.Version)
		}, rxgo.WithContext(ctx4))

		fmt.Printf("%d) CLOSED\n", i)
		wg.Done()
	}(4)

	wg.Add(1)
	go func(i int) {
		<-obs2.DoOnNext(func(item interface{}) {
			v := item.(ValueVersion)
			fmt.Printf("%d) Ricevuto %d / %d\n", i, v.Val, v.Version)
		}, rxgo.WithContext(ctx5))

		fmt.Printf("%d) CLOSED\n", i)
		wg.Done()
	}(5)

	time.Sleep(100 * time.Millisecond)
	fmt.Printf("Numero Goroutine: %d\n", runtime.NumGoroutine())

	fmt.Println("\nEmetto 5")
	ch <- rxgo.Of(ValueVersion{
		Val:     5,
		Version: count,
	})
	time.Sleep(250 * time.Millisecond)

	fmt.Println("\nEmetto 10")
	ch <- rxgo.Of(ValueVersion{
		Val:     10,
		Version: count,
	})
	time.Sleep(250 * time.Millisecond)

	fmt.Println("\nEmetto 15")
	ch <- rxgo.Of(ValueVersion{
		Val:     15,
		Version: count,
	})
	time.Sleep(250 * time.Millisecond)

	fmt.Println("\nEmetto 20")
	ch <- rxgo.Of(ValueVersion{
		Val:     20,
		Version: count,
	})
	time.Sleep(250 * time.Millisecond)

	time.Sleep(100 * time.Millisecond)
	fmt.Printf("Numero Goroutine: %d\n", runtime.NumGoroutine())

	fmt.Println("\nChiudo ctx4")
	canc4()
	time.Sleep(250 * time.Millisecond)

	fmt.Println("\nChiudo ctx5")
	canc5()
	time.Sleep(250 * time.Millisecond)

	time.Sleep(100 * time.Millisecond)
	fmt.Printf("Numero Goroutine: %d\n", runtime.NumGoroutine())

	fmt.Println("\nChiudo CH")
	close(ch)
	time.Sleep(250 * time.Millisecond)

	time.Sleep(100 * time.Millisecond)
	fmt.Printf("Numero Goroutine: %d\n", runtime.NumGoroutine())

	wg.Wait()
}

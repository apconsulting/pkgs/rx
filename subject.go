package rx

import (
	"container/ring"
	"sync"
	"sync/atomic"
)

type ISubject[T any] interface {
	IObservable[T]
	IObserver[T]
}

// Subject is a special type of Observable that allows values to be multicasted to many
// Observers. While plain Observables are unicast (each subscribed Observer owns an
// independent execution of the Observable), Subjects maintain a registry of Observers.
type Subject[T any] struct {
	Observable[T]

	subMutex sync.RWMutex
	subs     map[*Subscriber[T]]struct{}

	isClosed atomic.Bool
	Disposed chan struct{}

	// An instance of Empty[T]() used when someone attempts to subscribe to a closed subject
	empty IObservable[T]
}

// TODO: Vedere se implementare queste funzioni, al momento servono solo per soddisfare
// l'interfaccia IObserver. Tecnicamente un subject è si un Observer, ma uno molto
// particolare e non credo sia opportuno che le funzionalità di Next, Error e Complete
// possano essere modificate. Approfondire. In ogni caso questi metodi sono stati aggiunti
// per implementare le functional options in IObserver (cosi da estenderle a tutti gli
// Observer-like per cui ha senso, come ad esempio TapObserver. Subject è una vittima
// collaterale)

// UPDATE: Observer e TapObserver sono stati divisi e non ha più senso che i metodi
// facciano parte dell'interfaccia IObserver

// func (s *Subject[T]) setNextFunc(func(val T))      {}
// func (s *Subject[T]) setErrorFunc(func(err error)) {}
// func (s *Subject[T]) setCompleteFunc(func())       {}

// Assert implementation
var _ ISubject[any] = &Subject[any]{}
var _ IObservable[any] = &Subject[any]{}

func (s *Subject[T]) Next(val T) {
	if !s.isClosed.Load() {

		s.subMutex.RLock()
		defer s.subMutex.RUnlock()

		for sub := range s.subs {
			sub.Next(val)
		}
	}
}

func (s *Subject[T]) Error(err error) {
	if !s.isClosed.Swap(true) {
		close(s.Disposed)

		s.subMutex.Lock()
		defer s.subMutex.Unlock()

		for sub := range s.subs {
			sub.Error(err)
			delete(s.subs, sub)
		}
	}
}

func (s *Subject[T]) Complete() {
	if !s.isClosed.Swap(true) {
		close(s.Disposed)

		s.subMutex.Lock()
		defer s.subMutex.Unlock()

		for sub := range s.subs {
			sub.Complete()
			delete(s.subs, sub)
		}
	}
}

func (s *Subject[T]) registerNewSubscriber(sub *Subscriber[T]) {
	if s.isClosed.Load() {
		s.empty.Subscribe(s)
	}

	s.subMutex.Lock()
	s.subs[sub] = struct{}{}
	s.subMutex.Unlock()

	sub.AddTeardownLogic(func() {
		s.subMutex.Lock()
		delete(s.subs, sub)
		s.subMutex.Unlock()
	})
}

// Initializes a new empty Subject.
func NewSubject[T any]() *Subject[T] {
	subj := &Subject[T]{
		subs:     make(map[*Subscriber[T]]struct{}),
		Disposed: make(chan struct{}),
		empty:    Empty[T](),
	}

	subj.onSubscribeFunc = func(s *Subscriber[T]) func() {
		subj.registerNewSubscriber(s)
		return nil
	}

	return subj
}

// BehaviorSubject is a special type of Subject that, on subscription, immediately emits
// the last value emitted to the new subscriber.
type BehaviorSubject[T any] struct {
	*Subject[T]

	lastValue T
}

// Creates a new empty BehaviorSubject. The given value is the default value emitted on
// subscription
func NewBehaviorSubject[T any](value T) *BehaviorSubject[T] {
	subj := &BehaviorSubject[T]{}
	subj.Subject = NewSubject[T]()

	subj.lastValue = value

	subj.onSubscribeFunc = func(s *Subscriber[T]) func() {
		subj.registerNewSubscriber(s)

		s.Next(subj.lastValue)

		return nil
	}

	return subj
}

func (s *BehaviorSubject[T]) Next(val T) {
	s.lastValue = val
	s.Subject.Next(val)
}

// ReplaySubject is like BehaviorSubject but instead of emitting only the last value, it
// records multiple values and replays them to new subscribers
type ReplaySubject[T any] struct {
	*Subject[T]

	buffer *ring.Ring
}

func NewReplaySubject[T any](size int) *ReplaySubject[T] {
	subj := &ReplaySubject[T]{}
	subj.Subject = NewSubject[T]()

	subj.buffer = ring.New(size)

	subj.onSubscribeFunc = func(s *Subscriber[T]) func() {
		subj.registerNewSubscriber(s)

		subj.buffer.Do(func(v any) {
			if v != nil {
				val := v.(T)
				s.Next(val)
			}
		})

		return nil
	}

	return subj
}

func (s *ReplaySubject[T]) Next(val T) {
	s.buffer.Value = val
	s.buffer = s.buffer.Next()

	s.Subject.Next(val)
}

// Creates a Subject from a channel. Completes when the channel closes. Due to the nature
// of go channels, this observable can never emit errors, since they can't be sent through
// a typed channel.
func SubjectFromChan[T any](ch <-chan T) *Subject[T] {
	subj := NewSubject[T]()

	go func() {
		for {
			select {
			case val, ok := <-ch:
				if !ok {
					subj.Complete()
					return
				}

				subj.Next(val)
			case <-subj.Disposed:
				return
			}
		}
	}()

	return subj
}

// Creates a BehaviorSubject from a channel. Completes when the channel closes. Due to the
// nature of go channels, this observable can never emit errors, since they can't be sent
// through a typed channel.
func BehaviorSubjectFromChan[T any](startingValue T, ch <-chan T) *BehaviorSubject[T] {
	subj := NewBehaviorSubject(startingValue)

	go func() {
		for {
			select {
			case val, ok := <-ch:
				if !ok {
					subj.Complete()
					return
				}

				subj.Next(val)
			case <-subj.Disposed:
				return
			}
		}
	}()

	return subj
}

// Creates a ReplaySubject from a channel. Completes when the channel closes. Due to the
// nature of go channels, this observable can never emit errors, since they can't be sent
// through a typed channel.
func ReplaySubjectFromChan[T any](size int, ch <-chan T) *ReplaySubject[T] {
	subj := NewReplaySubject[T](size)

	go func() {
		for {
			select {
			case val, ok := <-ch:
				if !ok {
					subj.Complete()
					return
				}

				subj.Next(val)
			case <-subj.Disposed:
				return
			}
		}
	}()

	return subj
}
